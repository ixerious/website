const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')

const app = express();

app.use(express.json());
app.use(cors())
// app.use(bodyParser.urlencoded({ extended: true }));

app.use('/alias', require('./routes/alias'));

app.listen(3000)