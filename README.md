# Website

#### Requirements:
- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

#### Setup
1. Copy `.env.example` to `.env` and fill it out.
1. `npm install`
1. `webpack` (might need to install it globally and should be run after every change to update the app.js file)
1. `docker-compose up`